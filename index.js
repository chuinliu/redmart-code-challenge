(function() {
    'use strict';

    var defaultInputFileName = 'map.txt';
    var fs = require('fs');

    var MAX_X; // x axis boundary
    var MAX_Y; // y axis boundary
    var NODES_MAP = []; // store all nodes in input file
    var TRAVELLED_NODES; // save travelled path
    var BEST_PATH; // keep track best path
    var START_TIME; // execution start time
    var END_TIME; // execution end time

    function main() {
        var filePath = defaultInputFileName;

        fs.exists(filePath, function(exists) {
            if (exists) {
                processInputFile(filePath);
            } else {
                console.log('input file:', defaultInputFileName, 'not found');
                process.exit(1);
            }
        });
    }

    function processInputFile(filePath) {
        START_TIME = new Date().getTime();

        console.log('read input file');
        var readline = require('readline');
        var Stream = require('stream');
        var instream = fs.createReadStream(filePath);

        var outstream = new Stream();
        var rl = readline.createInterface(instream, outstream);
        var y = 0;

        // read input file line by line
        rl.on('line', function(line) {
            if (y === 0) {
                // get map dimension
                var mapSize = line.split(' ');
                MAX_X = mapSize[0] - 1;
                MAX_Y = mapSize[1] - 1;
            } else {
                var yIndex = y - 1;
                NODES_MAP.push([]);
                var xNodes = line.split(' ');
                var length = xNodes.length;
                for (var i = 0; i < length; i++) {
                    NODES_MAP[yIndex].push(parseInt(xNodes[i]));
                }
            }

            y++;
        });

        rl.on('close', function() {
            TRAVELLED_NODES = new Array(MAX_Y + 1);

            findBestPath();
            printResult();
            process.exit();
        });
    }

    function printResult() {
        var drop = (BEST_PATH[0].z - BEST_PATH[BEST_PATH.length - 1].z).toString();
        console.log('length =', BEST_PATH.length);
        console.log('drop =', drop);
        console.log('email:', BEST_PATH.length + drop + '@redmart.com');
        END_TIME = new Date().getTime();
        console.log('done, elapsed (ms):', END_TIME - START_TIME);
    }

    function createNextNode(x, y, z) {
        var newZ = NODES_MAP[y][x];

        // skip if next node's elevation is same or higher
        if (newZ >= z) {
            return null;
        }

        return {
            x: x,
            y: y,
            z: newZ
        };
    }

    function updateBestPath(currentPath) {
        if (!BEST_PATH) {
            BEST_PATH = currentPath;
        } else if (currentPath.length > BEST_PATH.length) {
            // update best path if path length is longer
            BEST_PATH = currentPath;
        } else if (currentPath.length === BEST_PATH.length) {
            // update best path if current path length is same but steeper
            if ((currentPath[0].z - currentPath[currentPath.length - 1].z) >
                (BEST_PATH[0].z - BEST_PATH[BEST_PATH.length - 1].z)) {
                BEST_PATH = currentPath;
            }
        }
    }

    function calculateNodeBestPath(paths) {
        var nodeBestPath;
        var pathLength = paths.length;

        for (var i = 0; i < pathLength; i++) {
            var path = paths[i];

            if (!nodeBestPath) {
                nodeBestPath = path;
            } else if (path.length > nodeBestPath.length) {
                nodeBestPath = path;
            } else if (path.length === nodeBestPath.length) {
                // choose steeper drop if path length is same
                if (path[path.length - 1].z < nodeBestPath[nodeBestPath.length - 1].z) {
                    nodeBestPath = path;
                }
            }
        }

        return nodeBestPath;
    }

    function getTravelledPath(x, y) {
        if (TRAVELLED_NODES[y] && TRAVELLED_NODES[y][x]) {
            return TRAVELLED_NODES[y][x];
        }

        return null;
    }

    function setTravelledPath(x, y, path) {
        if (!TRAVELLED_NODES[y]) {
            TRAVELLED_NODES[y] = [];
        }

        TRAVELLED_NODES[y][x] = path;
    }

    function findBestPath() {
        console.log('finding best path...');
        var yLength = NODES_MAP.length;

        for (var y = 0; y < yLength; y++) {
            var yNodes = NODES_MAP[y];
            var xLength = yNodes.length;

            for (var x = 0; x < xLength; x++) {
                var z = yNodes[x];

                // skip if path has been travelled
                var travelledPath = getTravelledPath(x, y);
                if (travelledPath) {
                    continue;
                }

                var path = findEachNodeBestPath(x, y, z);
                setTravelledPath(x, y, path);
                updateBestPath(path);
            }
        }
    }

    function findEachNodeBestPath(x, y, z) {
        var nextMoves = getNextMoves(x, y, z);
        var path = [{
            x: x,
            y: y,
            z: z
        }];

        // set travelled path and return path if no more possible moves
        if (!nextMoves) {
            setTravelledPath(x, y, path);
            return path;
        }

        var possiblePaths = [];
        var nextMovesLength = nextMoves.length;

        for (var i = 0; i < nextMovesLength; i++) {
            var nextMove = nextMoves[i];
            var travelledPath = getTravelledPath(nextMove.x, nextMove.y);
            if (travelledPath) {
                possiblePaths.push(travelledPath);
                continue;
            }

            possiblePaths.push(findEachNodeBestPath(nextMove.x, nextMove.y, nextMove.z));
        }

        var nodeBestPath = path.concat(calculateNodeBestPath(possiblePaths));
        setTravelledPath(x, y, nodeBestPath);
        return nodeBestPath;
    }

    function getNextMoves(x, y, z) {
        var nextMoves = [];
        var north = moveNorth(x, y, z);
        if (north) {
            nextMoves.push(north);
        }

        var south = moveSouth(x, y, z);
        if (south) {
            nextMoves.push(south);
        }

        var west = moveWest(x, y, z);
        if (west) {
            nextMoves.push(west);
        }

        var east = moveEast(x, y, z);
        if (east) {
            nextMoves.push(east);
        }

        if (nextMoves.length === 0) {
            return null;
        }

        return nextMoves;
    }

    function moveNorth(x, y, z) {
        var nextY = y - 1;
        if (nextY < 0) {
            return null;
        }

        return createNextNode(x, nextY, z);
    }

    function moveSouth(x, y, z) {
        var nextY = y + 1;
        if (nextY > MAX_Y) {
            return null;
        }

        return createNextNode(x, nextY, z);
    }

    function moveWest(x, y, z) {
        var nextX = x - 1;
        if (nextX < 0) {
            return null;
        }

        return createNextNode(nextX, y, z);
    }

    function moveEast(x, y, z) {
        var nextX = x + 1;
        if (nextX > MAX_X) {
            return null;
        }

        return createNextNode(nextX, y, z);
    }

    main();
})();
